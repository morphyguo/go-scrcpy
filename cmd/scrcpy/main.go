package main

import (
	"flag"
	"fmt"
	"github.com/flopp/go-findfont"
	"go-scrcpy/internal/app"
	"go-scrcpy/pkg"
	"go-scrcpy/pkg/log"
	"os"
	"strings"
)

func init() {
	fontPaths := findfont.List()
	for _, path := range fontPaths {
		//楷体:simkai.ttf
		//黑体:simhei.ttf
		if strings.Contains(path, "simhei.ttf") {
			fmt.Println(path)
			os.Setenv("FYNE_FONT", path) // 设置环境变量  // 取消环境变量 os.Unsetenv("FYNE_FONT")
			break
		}
	}
	fmt.Println("=============")
}

func main() {
	var debugLevel int
	var bitRate int
	var port int
	var mirror bool
	var overTcp bool

	flag.IntVar(&debugLevel, "log", 0, "日志等级设置")
	flag.IntVar(&bitRate, "bitrate", 8000000, "视频码率")
	flag.IntVar(&port, "port", 27183, "adb 端口号")
	flag.BoolVar(&mirror, "mirror", false, "是否开启镜像")
	flag.BoolVar(&overTcp, "overtcp", false, "通过局域网连接")
	flag.Parse()

	if overTcp && port == 27183 {
		port = 10240
	}

	option := app.Option{
		Debug:   pkg.DebugLevelWrap(debugLevel),
		BitRate: 8000000,
		Port:    port,
		Mirror:  mirror,
		OverTcp: overTcp,
	}
	log.Debug("%v", app.Run(&option))
}
