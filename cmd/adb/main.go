package main

import (
	"github.com/electricbubble/gadb"
	"log"
)

func main() {
	adbClient, err := gadb.NewClient()
	checkErr(err, "fail to connect adb java")

	devices, err := adbClient.DeviceList()
	checkErr(err)

	if len(devices) == 0 {
		log.Fatalln("list of devices is empty")
	}

	log.Println("install completed")

}

func checkErr(err error, msg ...string) {
	if err == nil {
		return
	}

	var output string
	if len(msg) != 0 {
		output = msg[0] + " "
	}
	output += err.Error()
	log.Fatalln(output)
}
