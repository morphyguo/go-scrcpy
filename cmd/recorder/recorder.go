package main

import (
	"flag"
	"go-scrcpy/internal/codec"
	"go-scrcpy/internal/sink"
	"go-scrcpy/internal/sink/img"
	"go-scrcpy/internal/source"
	"go-scrcpy/internal/source/tcp"
	"sync"
	"time"
)

var ip = flag.String("h", "", "device ip")
var port = flag.Int("p", 0, "device port")
var jarPath = flag.String("jar", "./scrcpy-server.jar", "scrcpy-server.jar path")

func main() {
	flag.Parse()
	if *ip == "" || *port == 0 {
		flag.Usage()
		return
	}
	ss, err := tcp.NewSourceTcCliClient(source.LocalPort(*port),
		source.Host(*ip),
		source.SockName("scrcpy"))
	if err != nil {
		panic(err)
	}
	factory, _, _ := codec.NewFrameFactory()
	pngSink := img.NewPngFileSink("./sc", sink.Width(ss.Format().Size.Width), sink.Height(ss.Format().Size.Height))
	decoder := codec.NewDecoder(factory, ss, pngSink)
	var wg sync.WaitGroup
	wg.Add(1)
	decoder.Start()
	go func() {
		<-time.After(time.Second * 30)
		wg.Done()
	}()
	wg.Wait()

}
