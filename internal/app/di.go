package app

import (
	"github.com/electricbubble/gadb"
	"github.com/google/wire"
	"go-scrcpy/internal/controller"
)

var ServiceSet = wire.NewSet(
	gadb.NewClient,
	controller.NewDeviceController,
)
