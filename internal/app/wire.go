//go:build wireinject
// +build wireinject

package app

import (
	"github.com/google/wire"
	"go-scrcpy/internal/controller"
	"go-scrcpy/internal/source"
)

type Injector struct {
	DeviceController *controller.DeviceController
}

func buildInjector(serverOpt source.ServerOption) (*Injector, func(), error) {
	panic(wire.Build(ServiceSet,
		wire.Struct(new(Injector), "*"),
	))
}
