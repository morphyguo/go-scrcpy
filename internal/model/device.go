package model

type Device struct {
	SerialName string
	Snapshot   []byte
}
