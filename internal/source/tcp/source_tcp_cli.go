package tcp

import (
	"fmt"
	"go-scrcpy/internal/source"
	"go-scrcpy/pkg"
	"go-scrcpy/pkg/log"
	"io"
	"net"
	"time"
)

type sourceTcCli struct {
	name       string
	options    source.Options
	screenSize pkg.Size
	conn       net.Conn
	close      bool
}

func (c *sourceTcCli) Name() string {
	return c.options.Serial
}

func (c *sourceTcCli) Format() pkg.MetaData {
	return pkg.MetaData{
		Size:   c.screenSize,
		Format: "h264",
	}
}

func (c *sourceTcCli) Init() error {
	buf := make([]byte, deviceNameLength+4)
	if _, err := io.ReadFull(c.conn, buf); err != nil {
		return err
	}

	//c.name = string(buf[:deviceNameLength])
	c.screenSize.Width = int(buf[deviceNameLength])<<8 | int(buf[deviceNameLength+1])
	c.screenSize.Height = int(buf[deviceNameLength+2])<<8 | int(buf[deviceNameLength+3])
	log.Info("device screen size name = %s, = %+v, %s", c.name, c.screenSize, buf)
	return nil
}

func (c *sourceTcCli) Input(buf []byte) (n int, err error) {
	if c.close {
		return 0, io.EOF
	}
	n, err = c.conn.Read(buf)
	if err != nil {
	}
	log.Info("input = %v", n)
	return
}

func (c *sourceTcCli) Send(b []byte) error {
	_, err := c.conn.Write(b)
	if err != nil {
		log.Error("err %+v", err)
	}
	return err
}

func (c *sourceTcCli) Conn() net.Conn {
	return c.conn
}

func (c *sourceTcCli) Close() error {
	c.close = true
	c.conn.Close()
	return nil
}

func (c *sourceTcCli) connectToRemote(attempts int, delay, timeout time.Duration) (err error) {
	for attempts > 0 {
		if err = c.connectAndReadByte(timeout); err == nil {
			return
		}
		time.Sleep(delay)
		attempts--
	}

	return
}

func (c *sourceTcCli) connectAndReadByte(timeout time.Duration) (err error) {
	if c.conn, err = net.DialTimeout(
		"tcp", fmt.Sprintf("%s:%d", c.options.Host, c.options.LocalPort), 5*time.Second); err != nil {
		log.Info("DialTimeout,option = %v, err = %v", c.options, err)
		return
	}
	if timeout > 0 {
		c.conn.SetReadDeadline(time.Now().Add(timeout))
		defer c.conn.SetReadDeadline(time.Time{})
	}

	// 只要 tunnel 建立（adb froward）建连就会成功，
	// 即使此时 device 上的 Device 还没有 listen。
	// 所以这里还要读取一个字节，保证 device 上的 Device 已经开始工作
	buf := make([]byte, 1)
	_, err = io.ReadFull(c.conn, buf)
	return
}

func NewSourceTcCliClient(opt ...source.Option) (source.Source, error) {
	o := source.Options{}
	for _, option := range opt {
		option(&o)
	}
	c := &sourceTcCli{
		options: o,
		conn:    nil,
		close:   false,
	}

	if err := c.connectToRemote(100, 100*time.Millisecond, 10*time.Second); err != nil {
		return nil, err
	}
	c.Init()
	return c, nil
}
