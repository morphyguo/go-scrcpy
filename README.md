# android多设备群控

android多设备投屏管理器

![演示](docs/demo.gif)

### 依赖

1. Golang 环境（设置 $GOPATH 环境变量）
2. ffmpeg
3. Android adb 工具
4. yaml
5. pkg-config 编译配置工具
6. cygwin(win10)
7. fyne

### 构建

#### win10

##### 安装

- cygwin64 从官网下载 setup.exe, 然后运行，proxy可以设置163的镜像，利用cygwin工具安装gcc-core, pkg-config, yasm,make, mingw64-gcc-core

https://www.baidu.com/link?url=4IVRiU2cha29FsDL4COCAKOusMnom8ycE3M6PNsVQuSaE4yCEVVGyP-Upf4iY5EvXgY7BB-Dr_Mc_Y8Z5grL8a&wd=&eqid=e02f43380002541c0000000661e978c0

- ffmpeg 下载
  https://ffmpeg.org/releases/ffmpeg-snapshot.tar.bz2

```
 ./configure --prefix=../ffmpeg_install --enable-shared --enable-cross-compile --arch=x86_64 --target-os=mingw64 --cross-prefix=x86_64-w64-mingw32-
 make -j 4
make install
```

##### 编译

依赖注入

```shell
CGO_CFLAGS="-I/include -L/lib" CGO_ENABLED=1 CC=x86_64-w64-mingw32-gcc GOOS=windows GOARCH=amd64 wire gen .
```

```
CGO_CFLAGS="-I/include -L/lib" CGO_ENABLED=1 CC=x86_64-w64-mingw32-gcc GOOS=windows GOARCH=amd64 go build  -v
CGO_CFLAGS="-I/include -I/usr/local/include -L/usr/local/lib -L/lib" CGO_ENABLED=1 CC=x86_64-w64-mingw32-gcc  go build  -o go-scrcpy.exe cmd/scrcpy/main.go
```

##### 打包

```
fyne package
```

## recorder演示

cmd/recorder是截屏功能演示。 使用tcp方式， 手机设备端作为tcp server， pc 侧作为client，实现手机屏幕抓取，存储为图片

```shell
# 编译 recorder, 在cgywin执行（前提是你已经编译了ffmpeg）
CGO_CFLAGS="-I/include -I/usr/local/include -L/usr/local/lib -L/lib" CGO_ENABLED=1 CC=x86_64-w64-mingw32-gcc GODEBUG=cgocheck=0 go build  -o recorder.exe cmd/recorder/recorder.go

# 编译 server jar, 如果没有android sdk 和gradle环境可以跳过此步.使用已经编译好的server jar
cd java
./gradlew build
cp server/build/outputs/apk/debug/server-debug.apk ../build/server-debug.jar

# 推送jar到device
adb push adb push ./build/server-debug.jar  /data/local/tmp/scrcpy.jar
# 启动 server tcp, listen
adb shell CLASSPATH=/data/local/tmp/scrcpy.jar app_process / com.genymobile.scrcpy.Server 720 8000000 :8080 false
#拔掉usb线

# 在PC上执行如下命令
mkdir sc #创建存储图片目录
./recorder.exe -h 192.168.xxxx -p 8080
```

### 联系

如果有问题，欢迎MR 或微信QQ: 405514071

