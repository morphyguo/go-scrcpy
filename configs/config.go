package global

import (
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"log"
)

type Config struct {
	Tcp Tcp `yaml:"tcp"`
}

type Tcp struct {
	Host string `yaml:"host"`
	Port string `yaml:"port"`
}

func Init(settingFile string) *Config {
	content, err := ioutil.ReadFile(settingFile)
	if err != nil {
		log.Fatalln(err)
	}
	var entryFile Config
	if err = yaml.Unmarshal(content, &entryFile); err != nil {
		log.Fatalln(err)
	}
	return &entryFile
}
