package pkg

import "errors"

var ErrEOF = errors.New("eof")
